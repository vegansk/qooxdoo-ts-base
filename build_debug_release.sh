#!/bin/bash

ROOT=`dirname $0`
SRC=$ROOT/build/script/base.js
APP_ROOT=$ROOT/source/class/base
RESULT=$ROOT/build/script/qooxdoo.js
RESULT_MIN=$ROOT/build/script/qooxdoo.min.js

set -e

rm -f $ROOT/build/script/*.js
rm -f $ROOT/source/script/*.js

./generate.py source-hybrid
./generate.py build

# Build debug version
sed -e 's/,"qx.debug":false,/,"qx.debug":true,/' $SRC > $RESULT
sed -i '/^qx.$$packageData\[/,$d' $RESULT
SCRIPTS=($(ls --sort=t -r $ROOT/source/script/base.*.*))

cat ${SCRIPTS[0]} >> $RESULT
cat $APP_ROOT/Application.js >> $RESULT
cat ${SCRIPTS[1]} >> $RESULT
cat $APP_ROOT/theme/Appearance.js >> $RESULT
cat $APP_ROOT/theme/Color.js >> $RESULT
cat $APP_ROOT/theme/Font.js >> $RESULT
cat $APP_ROOT/theme/Decoration.js >> $RESULT
cat $APP_ROOT/theme/Theme.js >> $RESULT
echo "" >> $RESULT
echo "" >> $RESULT
echo "qx.\$\$loader.init();" >> $RESULT

mv $SRC $RESULT_MIN

